<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Log;
use App\AppProject;
use App\Builds;
use App\AppType;
use App\BuildType;
use App\Project;

class BuildsView extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {
        $data = Builds::all();
        return view('build_view', compact('data'));
    }   


    public function search(Request $request)
    {

      
    }

       public function download(Request $request){
        $logs = new Log();
        $file_path = storage_path('app'.'/'.$request->project.'/'.$request->app.'/'.'builds'.'/'.$request->build.'/'.$request->file);
        $ip = \Request::getClientIp();
        $location = \Location::get($ip);
       
        if ($location != null) {
            $logs->ip_address = $location->ip;
            $logs->latitude = $location->latitude;
            $logs->longitude = $location->longitude;
            $logs->download_by = auth()->user()->name;
            $logs->file_name = $request->file;
            $logs->save();
        return response()->download($file_path);

        } else {
            $logs->ip_address = $ip;
            $logs->download_by = auth()->user()->name;
            $logs->file_name = $request->file;
            $logs->save();
        return response()->download($file_path);
        
        }    
    }  

    public function release_note(Request $request){
        $build = Builds::where('id',$request->id)->first();
        $note = $build->release_note;
        return response()->json(['status'=>true,'data'=>$note]);

    
    }  

 }

