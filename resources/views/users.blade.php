@extends('layouts.app')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Users List</div>
                <div class="panel-body">
                    <table class="table table-bordered table-hover " id="myTable">            
                        <thead>
                            <tr>
                                <td>Id</td>
                                <td>Name</td>
                                <td>Email</td>
                                <td>Role</td>
                                <td>Status</td>
                                <td>Action</td>
                                
                            </tr>
                        </thead>                        
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>{{$user->id }}</td>
                                <td>{{$user->name }}</td>
                                <td>{{$user->email}}</td>
                                @if($user->is_admin =='0')
                                <td>User 
                                </td>
                                @else
                                <td>Admin</td>
                                @endif
                                <td>@if($user->is_active=='0' && $user->is_admin =='0')<span>Inactive </span>                                    
                                 @elseif($user->is_active=='1' && $user->is_admin =='0')<span>Active</span> @endif
                                </td>
                                <td>@if($user->is_active=='0' && $user->is_admin =='0')
                                    <a onclick="activate('{{$user->id }}');" ><svg width="25px" hieght="25px" aria-hidden="true" focusable="false" data-prefix="fad" data-icon="toggle-on" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-toggle-on fa-w-18 fa-3x"><g class="fa-group"><path fill="#ffffff" d="M384 384a128 128 0 1 1 128-128 127.93 127.93 0 0 1-128 128z" class="fa-secondary"></path><path fill="#ff0000" d="M384 64H192C86 64 0 150 0 256s86 192 192 192h192c106 0 192-86 192-192S490 64 384 64zm0 320a128 128 0 1 1 128-128 127.93 127.93 0 0 1-128 128z" class="fa-primary"></path></g></svg></a>

                                    @elseif($user->is_active=='1' && $user->is_admin =='0')<a onclick="deactivate('{{$user->id }}');" ><svg width="25px" hieght="25px" aria-hidden="true" focusable="false" data-prefix="fad" data-icon="toggle-off" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-toggle-off fa-w-18 fa-3x"><g class="fa-group"><path fill="#44c70e" d="M384 64H192C86 64 0 150 0 256s86 192 192 192h192c106 0 192-86 192-192S490 64 384 64zM192 384a128 128 0 1 1 128-128 127.93 127.93 0 0 1-128 128z" class="fa-secondary"></path><path fill="#ffffff" d="M192 384a128 128 0 1 1 128-128 127.93 127.93 0 0 1-128 128z" class="fa-primary"></path></g></svg></a>
                                     @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="{{ asset('js/app.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js" ></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>

<script type="text/javascript">
    $(document).ready(function() {



  $( function() {
   $( "#datepicker" ).datepicker();
});



  var dtable = $('#myTable').DataTable({
    sDom: 'lrtip',
});
  });


    function activate(id)
{
  var url = "{{ route('user_activate')}}";  
  console.log(id);
    $.ajax({
        type: "post",
        dataType: 'json',
        url: url,
        data: {
            id: id,
        },
        success: function(data) {
          location.reload();
     },
        error: function(data) {
            console.log(data);
        }
    });
}   

 function deactivate(id)
{
  var url = "{{ route('user_deactivate')}}";  
    $.ajax({
        type: "post",
        dataType: 'json',
        url: url,
        data: {
            id: id,
        },
        success: function(data) {
          location.reload();
     },
        error: function(data) {
            console.log(data);
        }
    });
}
</script>