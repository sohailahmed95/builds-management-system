<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_project', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('app_type_id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->timestamps();


            $table->foreign('app_type_id')
                 ->references('id')->on('app_type')
                 ->onDelete('cascade');

            $table->foreign('project_id')
                 ->references('id')->on('new_project')
                 ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_project');
    }
}
