<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Builds extends Model
{
  	protected $table = "upload_build";
    protected $fillable = ['app_type_id','build_type_id','project_id','app_version','build_num','build_file','uploaded_by','release_note'];


  	 public function project()
    {
    	return $this->belongsTo('App\Project','project_id');
    }

 	 public function app()
    {
    	return $this->belongsTo('App\AppType','app_type_id');
    } 

    public function build()
    {
    	return $this->belongsTo('App\BuildType','build_type_id');
    }
}
