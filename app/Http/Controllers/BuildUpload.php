<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Carbon\Carbon;
use App\AppProject;
use App\AppType;
use App\Builds;
use App\BuildType;
use App\Project;

class BuildUpload extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $project;
        // $apptype;
        // $buildtype;
    }
	
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

     public function index()
    {
       $projects = Project::all();
       $app_types = AppType::all();
       $build_types = BuildType::all();
       return view('build',compact('app_types','build_types','projects'));
    }  


     public function on_change(Request $request)
    { 

        $apps = AppProject::select('app_type_id')->where('project_id',$request->project)->get();
        $types = AppType::find($apps);
        return response()->json(['status'=>true,'data'=>$types]);

    }  
    //  public function get_buildver(Request $request)
    // { 

    //     $project = Project::find($request->project)->project_name;
    //     $apptype = AppType::find($request->app)->app_type_name;
    //     $buildtype = BuildType::find($request->build)->build_type_name;
        
    //     if (file_exists(storage_path('app'.'/'.$project.'/'.$apptype.'/'.'builds'.'/'.$buildtype))) {

    //       $build_ver = Builds::select('build_version')->where(array(
    //       'project_id' => $request->project,
    //       'app_type_id' => $request->app,
    //       'build_type_id' => $request->build))->orderBy('id', 'desc')->first();
            
    //     return response()->json(['status'=>true,'data'=>$build_ver]);
    //   }
    // }  


    public function upload(Request $request)
    {

    	 $file_loc= "";
       $errmsg= "";
    	 $rules = [
            'project_name' => 'required|unique:new_project',
            'project_type' => 'required',
            'release_note' => 'required',
            'build_type' => 'required',
            'build_file' => 'required|mimetypes:application/java-archive,application/zip|max:200000'
        ];

        $errors = [
           'project_name.required' => 'Please select project',
           'project_type.required' => 'Please select project type',
           'release_note.required' => 'Please write a release note',
           'build_type.required' => 	'Please select build type',
           'build_file.required' => 	'Please select build file',
           'build_file.mimetypes' =>  'Please select apk/ipa/zip file only',
           'build_file.max'        =>  'Please select a file smaller than 200mb'
        ];

        $validator = Validator::make($request->all(), $rules, $errors);
		    $file = $request->file('build_file');
		        
        if ($validator->fails()) {

        return redirect()->back()->with('errors',$validator->errors())->withInput();          
        }

        $project = Project::find($request->input('project_name'))->project_name;
        $apptype = AppType::find($request->input('project_type'))->app_type_name;
        $buildtype = BuildType::find($request->input('build_type'))->build_type_name;

       	if($validator){

        if (file_exists(storage_path('app'.'/'.$project.'/'.$apptype))) {

           ini_set('display_errors', 1);
    $url = "https://upload.diawi.com/"; 
    $filename = storage_path('app/'.$file->getClientOriginalName());
    $tmp = Storage::put('app/public', $request->file($file->getClientOriginalName()));
        
    $file_loc = $file->move(storage_path('app'.'/'.$project.'/'.$apptype.'/'.'builds'.'/'.$buildtype), $file->getClientOriginalName());
    if ($file->getClientOriginalName() != '')
    {
        $headers = array("Content-Type: multipart/form-data"); // cURL headers for file uploading
        $postfields = array(
            "token"             => 'jJm6cMs70QUdbPA4331qbSfxyxh6Nm431hf4YFD0ab',
            "file"              => new \CurlFile(realpath($file_loc)),
            "find_by_udid"      => 0,
            "wall_of_apps"      => 1,
            "callback_email"    => 'itssohail97@gmail.com'
            );
        $ch = curl_init();

        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_HEADER => false,
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_POSTFIELDS => $postfields,
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36',
            CURLOPT_TIMEOUT => 400,
            CURLOPT_RETURNTRANSFER => 1
        ); 

        curl_setopt_array($ch, $options);
        $op = curl_exec($ch);

        if(!curl_errno($ch))
        {
            $info = curl_getinfo($ch);
            if ($info['http_code'] == 200){
            usleep(60000000);
            $errmsg = "File uploaded successfully";
            $job_details = json_decode($op); 
            $job_id = $job_details->job;
            $status_link = 'https://upload.diawi.com/status?token=jJm6cMs70QUdbPA4331qbSfxyxh6Nm431hf4YFD0ab&job='.$job_id;
            
            $ch2 = curl_init();
            curl_setopt($ch2, CURLOPT_URL, $status_link);
            curl_setopt($ch2, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded"));
            curl_setopt($ch2, CURLOPT_HEADER, 0);
            curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
            $op2 = curl_exec($ch2);
            $upload_response = json_decode($op2);
            if($upload_response->status == 2000){
                echo '<br>File uploaded successfully : download link ' . $upload_response->link;
            }
            curl_close($ch2);           
            }
        }
        else
        {
            $errmsg = curl_error($ch);
        }
        curl_close($ch);
    }
    else
    {
        $errmsg = "Please select the file";
    }
                dd($upload_response);

    die;
			


          $build = new Builds();
          $build->app_type_id = $request->input('project_type');
          $build->build_type_id = $request->input('build_type');
          $build->project_id = $request->input('project_name');
          $build->app_version =$request->input('app_version');
          $build->build_num =$request->input('build_num');
          $build->release_note =$request->input('release_note');
          $build->build_file = $file_loc->getFileName();
          $build->uploaded_by = auth()->user()->name;
          $build->save();

                 	
        }
        else{
          $request->session()->flash('alert-danger', 'Directory does not exist');
        return redirect()->back();

        }
}

        $request->session()->flash('alert-success', 'Build Uploaded Successfully!');
        return redirect()->back();
   


    }



}
