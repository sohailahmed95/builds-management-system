@extends('layouts.app')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Uploaded Builds Database</div>
                <div class="panel-body">
                    <table class="table table-bordered table-hover " id="myTable">
                        <thead>
                            <tr>
                                <th>Build No.</th>
                                <th>Project Name<br><input class="pfilter w-100 h-25" data-order="1" type="text" ></th>
                                <th>App Type<br><input class="afilter w-100 h-25" data-order="2" type="text"  ></th>
                                <th>Build Type<br><input class="bfilter w-100 h-25" data-order="3" type="text"  ></th>
                                <th>Build Version<br><input class="bvfilter w-100 h-25" data-order="4" type="text"  ></th>
                                <th>Uploaded By<br><input class="ufilter w-100 h-25" data-order="5" type="text"  ></th>
                                <th>Uploaded On<br><input class="dfilter w-100 h-25" id="datepicker" data-order="6" type="text"  ></th>
                                <th>File Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr class="par-tr">                                
                                <td>{{$item->id}}</td>
                                <td>{{$item->project->project_name}}</td>
                                <td>{{$item->app->app_type_name}}</td>
                                <td>{{$item->build->build_type_name}}</td>
                                <td>{{$item->build_version}}</td>
                                <td>{{$item->uploaded_by}}</td>
                                <td>{{\Carbon\Carbon::parse($item->created_at)->format('Y-m-d')}}</td>
                                <td>{{$item->build_file}}</td>
                                <td class="last-link">


                                    <a class="downloadurl"  data-toggle="tooltip" data-placement="top" title="Download File"   style="margin: 0px 0px 5px 10px;" href="{{ route('download',['project'=>$item->project->project_name, 'app' => $item->app->app_type_name, 'build' => $item->build->build_type_name, 'file' => $item->build_file ])}}"><i class="fas fa-download"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="download" class="svg-inline--fa fa-download fa-w-16" role="img"  width="15px" height="15px"xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="#505050" d="M216 0h80c13.3 0 24 10.7 24 24v168h87.7c17.8 0 26.7 21.5 14.1 34.1L269.7 378.3c-7.5 7.5-19.8 7.5-27.3 0L90.1 226.1c-12.6-12.6-3.7-34.1 14.1-34.1H192V24c0-13.3 10.7-24 24-24zm296 376v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V376c0-13.3 10.7-24 24-24h146.7l49 49c20.1 20.1 52.5 20.1 72.6 0l49-49H488c13.3 0 24 10.7 24 24zm-124 88c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20zm64 0c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20z"></path></svg></i></a> 



                                    <a class=""  data-placement="top" title="Release Note"  onclick="get_note('{{$item->id}}')" data-toggle="modal" data-target="#myModal" style="margin: 0px 0px 5px 10px;" href="#"><i class="far fa-sticky-note"><svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="sticky-note" class="svg-inline--fa fa-sticky-note fa-w-14" width="15px" height="15px"role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="#ababab" d="M448 348.106V80c0-26.51-21.49-48-48-48H48C21.49 32 0 53.49 0 80v351.988c0 26.51 21.49 48 48 48h268.118a48 48 0 0 0 33.941-14.059l83.882-83.882A48 48 0 0 0 448 348.106zm-128 80v-76.118h76.118L320 428.106zM400 80v223.988H296c-13.255 0-24 10.745-24 24v104H48V80h352z"></path></svg></i></a>


                                    <a class="shorturl"  id="text" data-toggle="tooltip" data-placement="top" title="Copy URL" style="margin: 0px 0px 5px 10px;" href="#"><i class="fas fa-link"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="link" class="svg-inline--fa fa-link fa-w-16" width="15px" height="15px" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="#0099ff" d="M326.612 185.391c59.747 59.809 58.927 155.698.36 214.59-.11.12-.24.25-.36.37l-67.2 67.2c-59.27 59.27-155.699 59.262-214.96 0-59.27-59.26-59.27-155.7 0-214.96l37.106-37.106c9.84-9.84 26.786-3.3 27.294 10.606.648 17.722 3.826 35.527 9.69 52.721 1.986 5.822.567 12.262-3.783 16.612l-13.087 13.087c-28.026 28.026-28.905 73.66-1.155 101.96 28.024 28.579 74.086 28.749 102.325.51l67.2-67.19c28.191-28.191 28.073-73.757 0-101.83-3.701-3.694-7.429-6.564-10.341-8.569a16.037 16.037 0 0 1-6.947-12.606c-.396-10.567 3.348-21.456 11.698-29.806l21.054-21.055c5.521-5.521 14.182-6.199 20.584-1.731a152.482 152.482 0 0 1 20.522 17.197zM467.547 44.449c-59.261-59.262-155.69-59.27-214.96 0l-67.2 67.2c-.12.12-.25.25-.36.37-58.566 58.892-59.387 154.781.36 214.59a152.454 152.454 0 0 0 20.521 17.196c6.402 4.468 15.064 3.789 20.584-1.731l21.054-21.055c8.35-8.35 12.094-19.239 11.698-29.806a16.037 16.037 0 0 0-6.947-12.606c-2.912-2.005-6.64-4.875-10.341-8.569-28.073-28.073-28.191-73.639 0-101.83l67.2-67.19c28.239-28.239 74.3-28.069 102.325.51 27.75 28.3 26.872 73.934-1.155 101.96l-13.087 13.087c-4.35 4.35-5.769 10.79-3.783 16.612 5.864 17.194 9.042 34.999 9.69 52.721.509 13.906 17.454 20.446 27.294 10.606l37.106-37.106c59.271-59.259 59.271-155.699.001-214.959z"></path></svg></i></a>


                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Release Note</h4>
              </div>
              <div class="modal-body">
                  <p>No Release Note!</p>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
          </div>

      </div>
  </div>


</div>
</div>
@endsection


 

<script src="{{ asset('js/app.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js" ></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
<script type="text/javascript">
 
$(document).ready(function() {
    $('#myTable .last-link .shorturl').on('click', function() {
        var url = $(this).siblings('.downloadurl').attr('href');
        var urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        var urltest = urlRegex.test(url);
        if (urltest) {
            bit_url(url);
        } else {
            alert("Invalid URL");
        }
        function bit_url(url) {
            var url = url;
            var username = "sohail97";
            var key = "R_339fa44fdfeb421e9526afca0071cfd5";
            $.ajax({
                url: "http://api.bit.ly/v3/shorten",
                data: {
                    longUrl: url,
                    apiKey: key,
                    login: username
                },
                dataType: "json",
                success: function(data) {
                    var $temp = $("<input>");
                    $("body").append($temp);
                    $temp.val(data.data.url).select();
                    document.execCommand("copy");
                    $temp.remove();
                    $('#text').attr('title', 'Copied');
                    alert("Copied");
                },
                error: function(data) {
                    console.log(data)
                }
            });
        }
    });
});

function get_note(id) {

    $.ajax({
        type: "post",
        dataType: 'json',
        url: '{{ route('release_note') }}',
        data:{
            id:id
        },
        success: function(data) {
            $('.modal.fade').addClass('in').css('background-color', 'rgb(0, 0, 0, 0.7)');
            $('.modal-body p').html(data.data);    
        },
        error: function(data) {
            console.log('Something is wrong');
        }
    });
}


$(document).ready(function() {



  $( function() {
   $( "#datepicker" ).datepicker();
});



  var dtable = $('#myTable').DataTable({
    sDom: 'lrtip',
    "bSort" : false
});

  $('.pfilter').on('keyup', function() {
    if (this.value.length) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
    if (!$(this).val()) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
});


  $('.afilter').on('keyup', function() {
    if (this.value.length) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
    if (!$(this).val()) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
});

  $('.bfilter').on('keyup', function() {
    if (this.value.length) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
    if (!$(this).val()) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
});

  $('.bvfilter').on('keyup', function() {
    if (this.value.length) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
    if (!$(this).val()) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
});


  $('.ufilter').on('keyup', function() {
    if (this.value.length) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
    if (!$(this).val()) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
}); 

  $('.dfilter').on('change', function() {
      if(this.value.length){
        var date = new Date(this.value);
        console.log(date);
        let formatted_date = date.getFullYear() + "-" + (date.getMonth()+1) + "-"+("0" + date.getDate()).slice(-2)
        dtable.column($(this).attr('data-order')).search(formatted_date, true);
        dtable.draw();
    }
    if (!$(this).val()) {
       dtable.column($(this).attr('data-order')).search('', true);
       dtable.draw();
   }
});


});

</script>