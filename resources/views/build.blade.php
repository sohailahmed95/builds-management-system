@extends('layouts.app')

<link href="{{ asset('css/fstdropdown.css') }}" rel="stylesheet">

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Upload Build</div>
                <div class="panel-body">

                     <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                        @endforeach
                        @if( isset($errors) && count($errors) )
                        <div class="alert alert-danger">
                            <ul>
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                @foreach($errors->all() as $error)
                                <li>
                                    {{ $error }}
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </div>
                    <form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{ route('UploadBuild')}}">
                        
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="project_name">Project Name:</label>
                            <div class="col-sm-10">
                                <select class="fstdropdown-select form-control" onchange="getproject(this);"id="project_name" name="project_name" >
                                    <option  value="" selected hidden>Select Project</option>
                                    @foreach ($projects as $project)
                                    <option  value="{{$project->id}}" @if (old('project_name') == $project->id) selected="selected" @endif>{{ $project->project_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                          <div class="form-group">
                            <label class="control-label col-sm-2" for="project_type">App Type:</label>
                            <div class="col-sm-10">
                               <select class="form-control" id="project_type" name="project_type"disabled>
                                    <option  value=""selected hidden>Select Type</option>

                                {{--     @foreach ($app_types as $app_type)
                                    <option  value=""selected hidden>Select Type</option>
                                    <option  value="{{$app_type->id}}" @if (old('project_type') == $app_type->id) selected="selected" @endif>{{ $app_type->app_type_name }}</option>
                                    @endforeach --}}
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="build_type">Build Type:</label>
                            <div class="col-sm-10">
                                <select class="form-control" onchange="getBuildVer();" id="build_type" name="build_type" disabled>
                                    @foreach ($build_types as $build_type)
                                    <option  value=""selected hidden>Select Type</option>
                                    <option  value="{{$build_type->id}}" @if (old('build_type') == $build_type->id) selected="selected" @endif>{{ $build_type->build_type_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">File Upload:</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control-file" name="build_file" id="build_file">
                            </div>
                        </div> 
                           <div class="form-group">
                            <label class="control-label col-sm-2">Release Notes:</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="release_note" id="release_note" cols="30" rows="2" placeholder="Release Notes!" >{{ old('release_note')}}</textarea>
                            </div>
                        </div> 
                          <div class="form-group">
                            <label class="control-label col-sm-2" for="app_version">App Version:</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control font-20" name="app_version" id="app_version" >
                            </div>
                            
                        </div>
                             <div class="form-group">
                            <label class="control-label col-sm-2" for="build_num">Build #:</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control font-20" name="build_num" id="build_num" >
                            </div>
                            
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-offset-10 col-sm-2">
                                <button type="submit" class="btn btn-success">Upload</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
    <script src="{{ asset('js/fstdropdown.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js" ></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
<script src="//cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
<script type="text/javascript">

   $(document).ready(function() {
    $("input.fstsearchinput").attr('placeholder', "Search here!");
    
    var projectname = "{{old('project_name')}}";
       
       if (projectname != "") {

        var url = "{{route('on_change')}}";  
        $.ajax({
        type: "post",
        dataType: 'json',
        url: url,
        data: {
            project: projectname,
        },
        success: function(data) {

        var option = '';
            var data = data.data;

        Object.entries(data).forEach(entry => {
     
                option += "<option value="+data[entry[0]].id+">" + data[entry[0]].app_type_name + "</option>";
                $("#project_type").html(option);
        });
                     
     },
        error: function(data) {
            console.log('error');
        }
     });
        $("#project_type").prop('disabled', false);
        $("#build_type").prop('disabled', false);
     } 

    CKEDITOR.replace( 'release_note', {
        toolbar: [
        [ 'Cut', 'Copy', 'Paste', 'Undo', 'Redo', 'Clipboard' ],
        { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
        { name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
        '/',
        { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
        { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
        { name: 'basicstyles', items: [ 'Bold', 'Italic' ] },
        { name: 'paragraph',  items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] }
        ]
    });
});
function getproject($id)
{
    var url = "{{ route('on_change')}}";  
    $.ajax({
        type: "post",
        dataType: 'json',
        url: url,
        data: {
            project: $('#project_name').val(),
        },
        success: function(data) {
        var option = '';
            var data = data.data;

        Object.entries(data).forEach(entry => {
     
                option += "<option value="+data[entry[0]].id+">" + data[entry[0]].app_type_name + "</option>";
                $("#project_type").html(option);
        });
        $("#project_type").prop('disabled', false);
        $("#build_type").prop('disabled', false);          
     },
        error: function(data) {
            console.log('error');
        }
    });
}

// function getBuildVer()

// {
//      var url = "{ route('get_buildver')}}";  
//       $.ajax({
//         type: "post",
//         dataType: 'json',
//         url: url,
//         data: {
//             project: $('#project_name').val(),
//             app: $('#project_type').val(),
//             build: $('#build_type').val()
//         },
//         success: function(response) {

//             if(response.data   == null){
//                 $("#build_version").val(0.1);
//             }
//             else{
//                 var version = response.data.build_version+0.1
//                 $("#build_version").val(parseFloat(version.toFixed(2)));
//             }

//      },
//         error: function(data) {
//             console.log(data);
//         }
//     });
// }
</script>