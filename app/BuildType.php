<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuildType extends Model
{
     protected $table = "build_type";
    	protected $fillable = ['build_type_name'];

}
