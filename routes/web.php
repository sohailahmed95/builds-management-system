<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/new-project', 'HomeController@index')->name('NewProject');
Route::get('/save-project', 'HomeController@create_project')->name('CreateProject');

Route::get('/upload-build', 'BuildUpload@index')->name('upload-build');
Route::post('/on_change', 'BuildUpload@on_change')->name('on_change');
Route::post('/get_buildver', 'BuildUpload@get_buildver')->name('get_buildver');
// Route::post('/get_buildver', 'BuildUpload@get_buildver')->name('get_buildver');

Route::post('/upload', 'BuildUpload@upload')->name('UploadBuild');
Route::get('/download/{project?}/{app?}/{build?}/{file?}', 'BuildsView@download')->name('download');
// Route::get('/download/{project?}/{app?}/{build?}/{file?}', 'BuildsView@short_link')->name('short_link');



Route::get('/short_link/{project?}/{app?}/{build?}/{file?}', 'BuildsView@short_link')->name('short_link');

Route::get('/build_view', 'BuildsView@index')->name('BuildsView');
Route::post('/search', 'BuildsView@search')->name('search');
Route::post('/release_note', 'BuildsView@release_note')->name('release_note');


Route::get('/users', 'Users@index')->name('userslist');
Route::get('/logs', 'Logs@index')->name('logs');

Route::post('/user_activate', 'Users@user_activate')->name('user_activate');
Route::post('/user_deactivate', 'Users@user_deactivate')->name('user_deactivate');
