@extends('layouts.app')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Logs</div>
                <div class="panel-body">
                    <table  class="table table-bordered table-hover " id="myTable">            
                        <thead>
                            <tr>
                                <td>Id</td>
                                <td>IP Address<br><input class="ipfilter w-100 h-25" data-order="1" type="text" ></td>
                                <td>Latitude</td>
                                <td>Longitude</td>
                                <td>Downloaded By<br><input class="userfilter w-100 h-25" data-order="4" type="text" ></td>
                                <td>File Name<br><input class="filefilter w-100 h-25" data-order="5" type="text" ></td>
                                <td>Date<br><input class="datefilter w-100 h-25" data-order="6" id="datepicker" type="text" ></td>                                
                            </tr>
                        </thead>                        
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{$item->id }}</td>
                                <td>{{$item->ip_address }}</td>
                                <td>{{$item->latitude }}</td>
                                <td>{{$item->longitude}}</td>                             
                                <td>{{$item->download_by}}</td>                             
                                <td>{{$item->file_name}}</td>                             
                                <td>{{\Carbon\Carbon::parse($item->created_at)->format('Y-m-d')}}</td>                            
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="{{ asset('js/app.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js" ></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>

<script type="text/javascript">

$(document).ready(function() {



      $( function() {
         $( "#datepicker" ).datepicker();
  });
   


    var dtable = $('#myTable').DataTable({
        sDom: 'lrtip',
        "bSort" : false
    });

    $('.ipfilter').on('keyup', function() {
        if (this.value.length) {
            dtable.column($(this).attr('data-order')).search(this.value, true);
            dtable.draw();
        }
        if (!$(this).val()) {
            dtable.column($(this).attr('data-order')).search(this.value, true);
            dtable.draw();
        }
    });


    $('.userfilter').on('keyup', function() {
        if (this.value.length) {
            dtable.column($(this).attr('data-order')).search(this.value, true);
            dtable.draw();
        }
        if (!$(this).val()) {
            dtable.column($(this).attr('data-order')).search(this.value, true);
            dtable.draw();
        }
    });

    $('.filefilter').on('keyup', function() {
        if (this.value.length) {
            dtable.column($(this).attr('data-order')).search(this.value, true);
            dtable.draw();
        }
        if (!$(this).val()) {
            dtable.column($(this).attr('data-order')).search(this.value, true);
            dtable.draw();
        }
    });

  

    $('.datefilter').on('change', function() {
          if(this.value.length){
            var date = new Date(this.value);
            console.log(date);
            let formatted_date = date.getFullYear() + "-" + (date.getMonth()+1) + "-"+("0" + date.getDate()).slice(-2)
            dtable.column($(this).attr('data-order')).search(formatted_date, true);
            dtable.draw();
          }
        if (!$(this).val()) {
             dtable.column($(this).attr('data-order')).search('', true);
            dtable.draw();
        }
    });


});
</script>