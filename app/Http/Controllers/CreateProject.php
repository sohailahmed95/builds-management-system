<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\ProjectType;

class CreateProject extends Controller
{

	  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
    
    } 


    public function show_project_types(){
    	
    	$types = ProjectType::all();
        return view('create-project',compact('types'));

    }
}
