<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class Users extends Controller
{
   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
   public function index(){

   		$users = User::all();
        return view('users',compact('users'));
   }

       public function user_activate(Request $request)
    { 
        $user = User::where('id',$request->id)->first();
        if ($user->is_active  == 0) {
        
          $user->is_active = 1;
          $user->save();
        } 
        return response()->json(['status'=>true,'data'=>$user]);
    }       

      public function user_deactivate(Request $request)
    { 
        $user = User::where('id',$request->id)->first();
        if ($user->is_active  == 1) {
        
          $user->is_active = 0;
          $user->save();
        } 
        return response()->json(['status'=>true,'data'=>$user]);
    }  
}
