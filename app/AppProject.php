<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppProject extends Model
{
		protected $table = "app_project";
    	protected $fillable = ['app_type_id','project_id'];
   
}
