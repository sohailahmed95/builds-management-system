<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadBuildTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_build', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('app_type_id')->unsigned();
            $table->integer('build_type_id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->text('release_note');
            $table->integer('app_version');
            $table->integer('build_num');
            $table->string('build_file',100);
            $table->string('uploaded_by',50);
            $table->timestamps();

            $table->foreign('app_type_id')
                 ->references('id')->on('app_type')
                 ->onDelete('cascade');

            $table->foreign('build_type_id')
                 ->references('id')->on('build_type')
                 ->onDelete('cascade');

            $table->foreign('project_id')
                 ->references('id')->on('new_project')
                 ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upload_build');
    }
}
