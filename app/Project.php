<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
		protected $table = "new_project";
    	protected $fillable = ['project_name'];
 
   public function app_type()
    {
        return $this->hasMany('App\AppType');
    }
    
}
