@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create New Project</div>
                <div class="panel-body">
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                        @endforeach
                        @if( isset($errors) && count($errors) )
                        <div class="alert alert-danger">
                            <ul>
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                @foreach($errors->all() as $error)
                                <li>
                                    {{ $error }}
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </div>
                    <form class="form-horizontal" id="newproject" action="{{ route('CreateProject')}}">
                        
                        {{ csrf_field() }}
                        
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="project_name">Project Name:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" value="{{ old('project_name')}}" id="project_name" name="project_name" placeholder="Enter Project Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="project_type">Project Type:</label>
                            <div class="col-sm-10">
                                {{-- <select class="form-control" id="project_type" name="project_type"> --}}
                                    {{-- @foreach ($types as $type) --}}
                                    
                                    <div class="form-check form-check-inline">                                        
                                        <label class="form-check-label" for="project_type" >Web</label>
                                        <input class="form-check-input" type="checkbox" id="project_type" name="project_type[]" value="1" {!! old("project_type") == 1 ? 'checked="checked"' : '' !!}>


                                        <label class="form-check-label" for="project_type" >Andriod</label>
                                        <input class="form-check-input" type="checkbox" id="project_type" name="project_type[]" value="2" {!! old("project_type")  == 2 ? 'checked="checked"' : '' !!}>
                                        
                                        <label class="form-check-label" for="project_type" >iOS</label>
                                        <input class="form-check-input" type="checkbox" id="project_type" name="project_type[]" value="3" {!! old("project_type") == 3 ? 'checked="checked"' : '' !!}>
                                    </div>
                                    
                                    {{-- @endforeach --}}
                                {{-- </select> --}}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-offset-10 col-sm-2">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js" ></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script>
$(window).load(function() {
$("div.flash-message").remove();
});


$(document).ready(function() {
$('#newproject').validate({
rules: {
'project_type[]': {
required: true,
minlength: 1
},
'pname': {
required: true,
minlength: 2
}
},
messages: {
'project_type[]': {
required: "You must check at least 1 box",
},
'pname': {
required: "Name is required",
}
}
});
});
</script>