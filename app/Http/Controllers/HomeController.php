<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\AppProject;
use App\AppType;
use App\Project;

class HomeController extends Controller
{
      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $types = ProjectType::all();
        
        return view('create-project');
    }

      public function create_project(Request $request)
    {   

        $rules = [
            'project_name' => 'required|unique:new_project',
            'project_type' => 'required'
        ];
       
        $errors = [
           'project_name' => 'Please enter project name',
           'project_type' => 'Please enter project type'
        ];

        $validator = Validator::make($request->all(), $rules, $errors);

        if ($validator->fails()) {
        return redirect()->back()->with('errors',$validator->errors())->withInput();               
        }
      
        $project_types = $request->input('project_type');
        $project = new Project();
        $project->project_name = $request->input('project_name');
        $project->save();        
        $types;


        foreach ($project_types as $project_type) {
        $type = AppType::find($project_type);
        $pivot = new AppProject();
        $pivot->app_type_id = $type['id'];
        $pivot->project_id =  $project->id;
        $pivot->save();

        if (!Storage::exists($project->project_name."/".$type->app_type_name)) {
        Storage::makeDirectory($project->project_name."/".$type->app_type_name."/"."builds"."/".'Alpha', 0777, true, true);
        Storage::makeDirectory($project->project_name."/".$type->app_type_name."/"."builds"."/".'Beta', 0777, true, true);
        Storage::makeDirectory($project->project_name."/".$type->app_type_name."/"."builds"."/".'UAT', 0777, true, true);
        }
    }
        $request->session()->flash('alert-success', 'New Project Created!');
        return redirect()->back();
    }


      public function build_view()
    {
        return view('build_view');
    }

}
