<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    
    protected $table = "download_logs";
    protected $fillable = ['ip_address','latitude','longitude','download_by','file_name'];
    
}