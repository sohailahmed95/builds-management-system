<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppType extends Model
{
     protected $table = "app_type";
    	protected $fillable = ['app_type_name'];
    

    public function project()
    {
    	return $this->belongsTo('App\Project');
    }
}
